# Pre-requisitos

## Docker

### Instalación Docker

1. Actualización de base de datos de paquetes

```
$ sudo apt-get update
```
<br>

2. Instalar dependencias y paquetes previos.

```
$ sudo apt-get install apt-transport-https ca-certificates curl
gnupg2 software-properties-common
```
<br>

3. la llave GPG oficial

```
$ sudo curl -fsSL https://download.docker.com/linux/debian/gpg |
sudo apt-key add -
```
<br>

4. Para reconocer el `add-apt-repository`

```
sudo apt-get install software-properties-common
```
<br>

5. Añadimos el repositorio ‘stable’

```
$ sudo add-apt-repository "deb [arch=amd64]
https://download.docker.com/linux/ubuntu $(lsb_release -cs)
stable"
```
<br>

6. Actualizar base de datos de paquetes

```
$ sudo apt-get update
```
<br>

### Docker Engine Community - Instalación

<br>

1. Instalar docker engine

```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Verificar Instalación

```
$ sudo docker run hello-world
```

<br>

## Kubernetes

### Instalación Kubernetes

Instalación del Snappy

```
$ sudo apt update && sudo apt install snapd
```
<br>

Se recomienda instalar los siguientes paquetes:
```
$ sudo apt install git vim net-tools openssh-server curl gnupg2
```
<br>

Instalar microk8s

```
$ sudo snap install microk8s --classic
```
<br>

Grupos de privilegio de administrador

```
$ sudo usermod -a -G microk8s $USER
$ sudo chown -f -R $USER ~/.kube
```
<br>

Status de Instalación y servicios activos

```
$ microk8s status --wait-ready
```

<br>

### ALIAS

En Kubernetes, el comando `microk8s` es utilizado para administrar el cluster; encuanto el `kubectl` se usa para administrar Kubernetes. 
Para facilicar nuestro trabajo con linea de comandos. Utilizaremos el alias `kubectl` para referirse a `microk8s kubectl`. 

<br>

Para ello, en el terminal de linux ejecutamos:
```
$ echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
```
<br>

Para aplicar los cambios (puede hacerse necesario un reinicio de sesión para efectivarlo):

```
$ source ~/.bashrc
```
<br>







