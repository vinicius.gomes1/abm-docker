# Dockerfile

FROM ubuntu:latest

# LABELS
LABEL maitainer="Vinicius Gomes"
LABEL version="3.3"
LABEL description="Final - Sitio Estatico (php) con base de datos Mysql"

# VARIABLES PARA PASAR ARGUMENTO 
ARG DEBIAN_FRONTEND=noninteractive
 
# Instalacion y configuracion de Stack Lamp
RUN apt update && apt install -y wget apache2 mariadb-server php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip vim w3m w3m-img && apt autoremove && apt clean

# Variables de entorno para el ServerName y ServerApp 
ENV APPSERVERNAME cafe.istea
ENV APPALIAS www.cafe.istea

# Variables de entorno para el mysql
ENV MYSQL_USER isteauser
ENV MYSQL_USER_PASSWORD 1234
ENV MYSQL_DB_NAME isteaabm
 
# Creamos el directorio 'app' en el raiz
RUN mkdir -p /app

# Copiar webpages al directorio 'app'
COPY eduvin.sql /app
COPY ./website/* /app/

# Dar permisos de escritura a la base de datos
RUN chmod +r+x+w /app/eduvin.sql

# Copiamos el fichero de vhost a 'sites-available'
COPY default.conf /etc/apache2/sites-available

# Exponemos el puerto 80
EXPOSE 80


# Copiamos el directorio 'apache2' de 'etc' hacia '/app/apache2'
# Si hay un punto de montaje hacia el directorio 'apache2'
# El entrypoint al encontrar vacio al directorio va a traer los ficheros 
# correspondientes de '/app/apache2'
RUN cp -r /etc/apache2 /app/apache2

# Definicion de los volumenes (para paginas, librerias y cofniguraciones de apache) 
VOLUME ["/var/www/html", "/var/lib/mysql", "/etc/apache2"]

# ENTRYPOINT 
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
