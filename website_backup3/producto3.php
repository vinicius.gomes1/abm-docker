<html>
<meta charset=UTF-8>
<title> Producto </title>
<body>
<h1> <u> <b> <font color="blue" face="Trebuchet MS,arial"> SISTEMA ABM DE PRODUCTOS </font> </b> </u> </h1>
<h1> <u> <b> Menú Producto</b> </u> </h1>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
<?php
$buscaproducto=utf8_decode($_POST["buscaproducto"]);
$buscaproductoid=utf8_decode($_POST["buscaproductoid"]);
$buscaproductonombre=utf8_decode($_POST["buscaproductonombre"]);
$buscaproductodescripcion=utf8_decode($_POST["buscaproductodescripcion"]);
$buscaproductoproveedor=utf8_decode($_POST["buscaproductoproveedor"]);
$buscaproductosubcategoria=utf8_decode($_POST["buscaproductosubcategoria"]);

$base='eduvin';
$tabla='producto';
$driver='mysql';
$hostname='localhost';
$username='root';
$password='';
$conexion = mysqli_connect($hostname, $username, $password, $base);

switch ($buscaproducto) {
    case "Buscar por ID":

if($buscaproductoid == ''){
	echo "Debe ingresar un ID de Producto.";
	break;
?>
<form NAME="botonatras">
	<INPUT TYPE="button" VALUE="Volver" onClick="history.back()">
	</form>
<?php
}
	else {
?>
<h2> <u> <b>Buscar Producto</b> </u> </h2>
<h3> <u> <b>Por ID</b> </u> </h3>
<?php
     if (!$conexion) {
      die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT producto.id, 
        producto.nombreproducto,
        producto.descripcionproducto, 
        subcategorias.nombresubcategoria as idsubcategoria, 
        proveedor.nombre as idproveedor, 
        moneda.simbolomoneda as idmoneda, 
        precio.numeroprecio as idprecio

FROM producto 

INNER JOIN subcategorias on subcategorias.id = producto.idsubcategoria 
INNER JOIN precio on precio.id = producto.idprecio 
INNER JOIN moneda on moneda.idmoneda= precio.idmoneda 
INNER JOIN proveedor on proveedor.id= producto.idproveedor where producto.id = $buscaproductoid";
//Variable para el resultado de la búsqueda
$id = '';
$nombreproducto = '';
$descripcionproducto = '';
$idsubcategoria = '';
$idproveedor = '';
$idprecio = '';
	//Variable para el número de registros encontrados
$registros = '';
$resultado = $conexion->query($sql);
	//Si hay resultados
		if (mysqli_num_rows($resultado) > 0){			
			// Registra el número de resultados
			echo "Se han encontrado " . mysqli_num_rows($resultado) . " registros" . "<br>" . "<br>";
			echo "<table><tr> <th> Id </th> <th> Nombre del Producto </th> <th> Descripción del Producto </th> <th> Subcategoría </th> <th> Proveedor </th> <th> Moneda </th> <th> Precio </th> </tr>";
			// Se almacenan las cadenas de resultado
			while($fila = mysqli_fetch_assoc($resultado)){
			// Se muestran los resultados en forma de tabla
			if($fila["idmoneda"]=='E') {
			$fila["idmoneda"] = "€";
			}
			echo "<tr><td>".$fila["id"]."</td><td>".$fila["nombreproducto"]."</td><td>".$fila["descripcionproducto"]."</td><td>".$fila["idsubcategoria"]."</td><td>".$fila["idproveedor"]."</td><td>".$fila["idmoneda"]."</td><td>".$fila["idprecio"]."</td></tr>";
			}
			echo "</table>";
	}
		else{				
				echo "Sin resultados en la base de datos";							
			}			
mysqli_close($conexion);
break;	
}
	case "Buscar por Nombre":

if($buscaproductonombre == ''){
echo "Debe ingresar un nombre de producto.";
break;
}
	else {

?>
<h2> <u> <b>Buscar Producto</b> </u> </h2>
<h3> <u> <b>Por Nombre</b> </u> </h3>
<?php
     if (!$conexion) {
      die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT producto.id, 
        producto.nombreproducto,
        producto.descripcionproducto, 
        subcategorias.nombresubcategoria as idsubcategoria, 
        proveedor.nombre as idproveedor, 
        moneda.simbolomoneda as idmoneda, 
        precio.numeroprecio as idprecio

FROM producto 

INNER JOIN subcategorias on subcategorias.id = producto.idsubcategoria 
INNER JOIN precio on precio.id = producto.idprecio 
INNER JOIN moneda on moneda.idmoneda= precio.idmoneda 
INNER JOIN proveedor on proveedor.id= producto.idproveedor where producto.nombreproducto like '%$buscaproductonombre%' ORDER BY producto.id";
//Variable para el resultado de la búsqueda
$id = '';
$nombreproducto = '';
$descripcionproducto = '';
	//Variable para el número de registros encontrados
$registros = '';
$resultado = $conexion->query($sql);
	//Si hay resultados
		if (mysqli_num_rows($resultado) > 0){			
			// Registra el número de resultados
			echo "Se han encontrado " . mysqli_num_rows($resultado) . " registros" . "<br>" . "<br>";
			echo "<table><tr> <th> Id </th> <th> Nombre del Producto </th> <th> Descripción del Producto </th> <th> Subcategoría </th> <th> Proveedor </th> <th> Moneda </th> <th> Precio </th> </tr>";
			// Se almacenan las cadenas de resultado
			while($fila = mysqli_fetch_assoc($resultado)){
			// Se muestran los resultados en forma de tabla
			if($fila["idmoneda"]=='E') {
			$fila["idmoneda"] = "€";
			}
			echo "<tr><td>".$fila["id"]."</td><td>".$fila["nombreproducto"]."</td><td>".$fila["descripcionproducto"]."</td><td>".$fila["idsubcategoria"]."</td><td>".$fila["idproveedor"]."</td><td>".$fila["idmoneda"]."</td><td>".$fila["idprecio"]."</td></tr>";
			}
			echo "</table>";
	}
		else{				
				echo "Sin resultados en la base de datos";							
			}			
mysqli_close($conexion);
break;
}
case "Buscar por Descripcion":
if($buscaproductodescripcion == ''){
	echo "Debe ingresar una descripción del producto.";
break;
}
	else {

?>
<h2> <u> <b>Buscar Producto</b> </u> </h2>
<h3> <u> <b>Por Descripción</b> </u> </h3>
<?php
     if (!$conexion) {
      die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT producto.id, 
        producto.nombreproducto,
        producto.descripcionproducto, 
        subcategorias.nombresubcategoria as idsubcategoria, 
        proveedor.nombre as idproveedor, 
        moneda.simbolomoneda as idmoneda, 
        precio.numeroprecio as idprecio

FROM producto 

INNER JOIN subcategorias on subcategorias.id = producto.idsubcategoria 
INNER JOIN precio on precio.id = producto.idprecio 
INNER JOIN moneda on moneda.idmoneda= precio.idmoneda 
INNER JOIN proveedor on proveedor.id= producto.idproveedor where producto.nombreproducto like '%$buscaproductodescripcion%' ORDER BY producto.id";
//Variable para el resultado de la búsqueda
$id = '';
$nombreproducto = '';
$descripcionproducto = '';
	//Variable para el número de registros encontrados
$registros = '';
$resultado = $conexion->query($sql);
	//Si hay resultados
		if (mysqli_num_rows($resultado) > 0){			
			// Registra el número de resultados
			echo "Se han encontrado " . mysqli_num_rows($resultado) . " registros" . "<br>" . "<br>";
			echo "<table><tr> <th> Id </th> <th> Nombre del Producto </th> <th> Descripción del Producto </th> <th> Subcategoría </th> <th> Proveedor </th> <th> Moneda </th> <th> Precio </th> </tr>";
			// Se almacenan las cadenas de resultado
			while($fila = mysqli_fetch_assoc($resultado)){
			// Se muestran los resultados en forma de tabla
			if($fila["idmoneda"]=='E') {
			$fila["idmoneda"] = "€";
			}
			echo "<tr><td>".$fila["id"]."</td><td>".$fila["nombreproducto"]."</td><td>".$fila["descripcionproducto"]."</td><td>".$fila["idsubcategoria"]."</td><td>".$fila["idproveedor"]."</td><td>".$fila["idmoneda"]."</td><td>".$fila["idprecio"]."</td></tr>";
			}
			echo "</table>";
	}
		else{				
				echo "Sin resultados en la base de datos";							
			}			
mysqli_close($conexion);
break;
}

case "Buscar por Subcategoria":
if($buscaproductosubcategoria == '')
{
	echo "Debe ingresar una Subcategoría.";
break;
}
	else {

?>
<h2> <u> <b>Buscar Producto</b> </u> </h2>
<h3> <u> <b> Por Subcategoría </b> </u> </h3>
<?php
     if (!$conexion) {
      die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT producto.id, 
        producto.nombreproducto,
        producto.descripcionproducto, 
        subcategorias.nombresubcategoria as idsubcategoria, 
        proveedor.nombre as idproveedor, 
        moneda.simbolomoneda as idmoneda, 
        precio.numeroprecio as idprecio

FROM producto 

INNER JOIN subcategorias on subcategorias.id = producto.idsubcategoria 
INNER JOIN precio on precio.id = producto.idprecio 
INNER JOIN moneda on moneda.idmoneda= precio.idmoneda 
INNER JOIN proveedor on proveedor.id= producto.idproveedor where producto.nombreproducto like '%$buscaproductosubcategoria%' ORDER BY producto.id";
//Variable para el resultado de la búsqueda
$id = '';
$nombreproducto = '';
$descripcionproducto = '';
	//Variable para el número de registros encontrados
$registros = '';
$resultado = $conexion->query($sql);
	//Si hay resultados
		if (mysqli_num_rows($resultado) > 0){			
			// Registra el número de resultados
			echo "Se han encontrado " . mysqli_num_rows($resultado) . " registros" . "<br>" . "<br>";
			echo "<table><tr> <th> Id </th> <th> Nombre del Producto </th> <th> Descripción del Producto </th> <th> Subcategoría </th> <th> Proveedor </th> <th> Moneda </th> <th> Precio </th> </tr>";
			// Se almacenan las cadenas de resultado
			while($fila = mysqli_fetch_assoc($resultado)){
			// Se muestran los resultados en forma de tabla
			if($fila["idmoneda"]=='E') {
			$fila["idmoneda"] = "€";
			}
			echo "<tr><td>".$fila["id"]."</td><td>".$fila["nombreproducto"]."</td><td>".$fila["descripcionproducto"]."</td><td>".$fila["idsubcategoria"]."</td><td>".$fila["idproveedor"]."</td><td>".$fila["idmoneda"]."</td><td>".$fila["idprecio"]."</td></tr>";
			}
			echo "</table>";
	}
		else{				
				echo "Sin resultados en la base de datos";							
			}			
mysqli_close($conexion);
break;
}



case "Buscar por Proveedor":
if($buscaproductoproveedor == '')
{
	echo "Debe ingresar un Proveedor.";
break;
}
	else {

?>
<h2> <u> <b>Buscar Producto</b> </u> </h2>
<h3> <u> <b>Por Proveedor</b> </u> </h3>
<?php
     if (!$conexion) {
      die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT producto.id, 
        producto.nombreproducto,
        producto.descripcionproducto, 
        subcategorias.nombresubcategoria as idsubcategoria, 
        proveedor.nombre as idproveedor, 
        moneda.simbolomoneda as idmoneda, 
        precio.numeroprecio as idprecio

FROM producto 

INNER JOIN subcategorias on subcategorias.id = producto.idsubcategoria 
INNER JOIN precio on precio.id = producto.idprecio 
INNER JOIN moneda on moneda.idmoneda= precio.idmoneda 
INNER JOIN proveedor on proveedor.id= producto.idproveedor where producto.nombreproducto like '%$buscaproductoproveedor%' ORDER BY producto.id";
//Variable para el resultado de la búsqueda
$id = '';
$nombreproducto = '';
$descripcionproducto = '';
	//Variable para el número de registros encontrados
$registros = '';
$resultado = $conexion->query($sql);
	//Si hay resultados
		if (mysqli_num_rows($resultado) > 0){			
			// Registra el número de resultados
			echo "Se han encontrado " . mysqli_num_rows($resultado) . " registros" . "<br>" . "<br>";
			echo "<table><tr> <th> Id </th> <th> Nombre del Producto </th> <th> Descripción del Producto </th> <th> Subcategoría </th> <th> Proveedor </th> <th> Moneda </th> <th> Precio </th> </tr>";
			// Se almacenan las cadenas de resultado
			while($fila = mysqli_fetch_assoc($resultado)){
			// Se muestran los resultados en forma de tabla
			if($fila["idmoneda"]=='E') {
			$fila["idmoneda"] = "€";
			}
			echo "<tr><td>".$fila["id"]."</td><td>".$fila["nombreproducto"]."</td><td>".$fila["descripcionproducto"]."</td><td>".$fila["idsubcategoria"]."</td><td>".$fila["idproveedor"]."</td><td>".$fila["idmoneda"]."</td><td>".$fila["idprecio"]."</td></tr>";
			}
			echo "</table>";
	}
		else{				
				echo "Sin resultados en la base de datos";							
			}			
mysqli_close($conexion);
break;
}
	default:
        break;
}
?>
<br> <br>
<form NAME="botonatras">
<INPUT TYPE="button" VALUE="Volver" onClick="history.back()">
</form>
</body>
</html>