# TUTORIAL DOCKER

# App ABM de productos en Kubernetes con imagen en Docker 
  
Realizaremos un despliegue de una aplicación para ABM de productos, escrita en php, cuya base de datos erá almacenada en MariaBD. 

En Kubernetes, el despliegue será del tipo Deployment, con 1 volumen persistente.  

El método incluye alta disponibilidad por medio de réplicas, a través de la estratégia RollingUgrade.

Y por último, se incluirá instrucciones para acceso interno via redireccionamiento de puertos.  

<br>

## Pre-requisitos
  
* Tener el microk8s instalado en una VM (utilizamos VirtualBox) con la versión reciente de Ubuntu. 
* Crear el un Alias para `microk8s kubectl`
* Bajar nuestro repositorio

Para más detalles leer `readme_prerequisitos.md`.
<br>

## Docker

Mostraremos como realizar la creación de la imagen y como subirla a DockerHub.
<br>

**Crear Imágenes a partir del Dockerfile**

```
$ docker build -t abmimage . 
```

**Correr el correr un contenedor de servidor inverso:**

```
$ docker run -d -p 80:80 \
-v /var/run/docker.sock:/tmp/docker.sock:ro \
jwilder/nginx-proxy
```

**Correr el contedor a partir de nuestra imagen**

```
$ docker run -d --name abmcontenedor \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-e MYSQL_ROOT_PASSWORD=bacaza123 \
-e MYSQL_USER=abmdbuser \
-e MYSQL_USER_PASSWORD=bagaza123 \
-e MYSQL_DB_NAME=eduvin \
-v /tmp/abm/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
abmimage
```

**Opciones:**
<br>`-d`: lo deja en segundo plano
<br>`--name`: para elegir el nombre del contenedor (registrocontendedor)
<br>`-e`: para pasar las variables de entorno
<br>`-v`: bind de montaje de un volumen
<br>`-p`: mapeamento de puertos (<host port>:<contenedor port>)


Para acceder al sitio hay que agregar el host `abm.istea`. En Windows lo encontrás en `C:\Windows\System32\drivers\etc\host`, y en linux debia está en `/etc/hosts`.

Luego, desde un navegador tipeás `http://abm.istea`.

<br>

## Subiendo una imagen al DockerHub

Lo prinmero es crear una imagen docker a partir de un contenedor. Está imagen puede contener modificaciones hechas despúes de que corremos el contenedor. 

```
$ docker commit abmcontenedor viniciusistea/abm:integrado
```
`abmcontenedor:` nombre del contenedor que usaremos como base
<br>
`viniciusistea/abm:` [nombre de la cuenta]/[nombre del repositorio]:[tag]
<br>
En nuestro caso la `tag` será el número de versionado.

<br>

Podemos ver la imagen creada con el comando abajo

```
$ docker images
```

<br>

Logueamos a nuestra cuenta de DockerHub, seguido de la password.

```
$ docker login
```

<br>

Subimos un nuevo repositorio chamado abm:

```
$ docker push viniciusistea/abm:integrado
```

`Obs: Para pushear una nueva tag a este repositorio:`

```
$ docker push viniciusistea/abm:tagname
```

<br>

Podemos bajar la imagen con el comando abajo:
```
$ docker pull viniciusistea/abm:integrado
```

O simplemente correr el contenedor directamente, mencionándola:

```
$ docker run -d --name abmcontenedor \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-e MYSQL_ROOT_PASSWORD=bacaza123 \
-e MYSQL_USER=abmdbuser \
-e MYSQL_USER_PASSWORD=bagaza123 \
-e MYSQL_DB_NAME=eduvin \
-v /tmp/abm/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
viniciusistea/abm:integrado
```
<br>

## Kubernetes

### 1. Preparando la infraestructura de datos persistentes

Como Kubernetes no tiene consistencia de datos, necesitamos crear volúmenes persistentes para preservalos. Caso contrario, todos los cambios realizados en la base de datos o cualquier otro directorio se perderia con el reinicio del contenedor. 
<br>

#### Creando PersistentVolumes (PV) para la aplicación ABM

```
$ kubectl apply -f abm-pv.yaml
```
<br>

Para listar los PVs creados:
```
$  kubectl get pv
```
<br>

#### Creando PersistentVolumeClaim (PVC)

```
$ kubectl apply -f abm-pvc.yaml
```
<br>

Listar los PVCs creados:
```
$  kubectl get pvc
```
<br>

### 2. Deployment en Kubernetes:

```
$ kubectl apply -f abm-deployment.yaml
```
<br>

Redirección de puertos para acceder al servicio

```
$ kubectl port-forward --address 0.0.0.0 deployment/abm 8080:80
```
<br>

Para probar a la aplicación, desde tu navegador, ingresar a:

http://abm.istea:8080

<br>

**Listooooooooooooooooo!**



<br>

## Comandos Importantes en Docker

##### Ver procesos/contenedores y sus estados:

```
$ docker ps -a
```

##### Listar contenedores por sus IDs:

```
$ docker ps -a -q
```

##### Eliminar contenedor:

```
$ docker rm <contenedor>
```

##### Eliminar todos los contenedores:

```
$ docker rm $(docker ps -a -q)
```

`$(docker ps -a -q)`: devuelve todos los IDs

##### Controlar contenedores:

```
$ docker start <container name>
$ docker stop <container name>
```

##### Listar imagenes: 

```
$ docker images
```

##### Borrar Imagenes:

```
$ docker rmi <image names>
```

##### Borrar todas imagenes:

a ) Parar todas los containers:

```
$ docker stop $(docker ps -a -q)
```

b ) Borrar todas las imagenes:

```
$ docker rmi $(docker images -q)
```

##### Executar comandos dentro de un contenedor:

```
$ docker exec -ti dockercontenedor /bin/bash 

```
<br>

## Comandos importantes en Kubernetes

##### Logs del pod

```
$ kubectl logs apache-cont
```

##### Lista pods

```
$ kubectl get pods
```

##### Lista de deployment

```
kubectl get deployment
```

##### Información detallada

```
$ kubectl get pod abm -o wide
```

`abm`: nombre del pod
Nos tira la IP del pod, además de otras infos

##### Otra alternativa (más detallada):

```
$ kubectl describe deployment/abm
```

##### Reverter deployments (Rollback)

```
$ kubectl rollout undo deployment/abm
```

##### Lista de pvc

```
$ kubectl get pvc
```

##### Lista de pv

```
$ kubectl get pv
```

##### Lista de servicios

```
$ kubectl get services
```

##### Lista de secrets

```
$ microk8s kubectl get secrets
```

##### Eliminar un pod: 

```
$ kubectl delete pod abm
```

##### Eliminar todos los pods y deployments

```
$ kubectl delete all --all --all-namespaces
```

##### Para ejecutar comandos:

```
kubectl exec -it abm --container abm -- /bin/bash
```
El primer `abm` se trata del objeto, y el segundo del contenedor. Dato importante si trabajamos con más de uno. Para nuestro caso vamos a tener sólo un contenedor

##### Buena aplicación para degub de yaml

```
yamllint
```























